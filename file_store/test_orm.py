from django.test import TestCase


class StoreTestCase(TestCase):

    def test_get_home(self):
        response = self.client.generic('GET', '/', host='localhost:8080')
        self.assertEqual(response.status_code, 200)

    def test_get_accounts(self):
        response = self.client.generic('GET', '/accounts/login/', host='localhost:8080')
        self.assertEqual(response.status_code, 200)
        response = self.client.generic('GET', '/accounts/logout/', host='localhost:8080')
        self.assertEqual(response.status_code, 200)

    def test_upload(self):
        response = self.client.generic('GET', '/uploads/', host='localhost:8080')
        self.assertEqual(response.status_code, 302)
