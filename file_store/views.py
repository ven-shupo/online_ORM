from django.views.decorators.csrf import csrf_exempt
from .class_orm_files import ORM_files
from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage


@csrf_exempt
def home(request):
    urls = ORM_files.get_all_document()
    return render(request, 'home.html', {'urls': urls})


@csrf_exempt
def upload(request):
    if not request.user.is_authenticated:
        return redirect('/accounts/login')
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        ORM_files.copy()
        return render(request, 'upload.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'upload.html')