import configparser
import psycopg2
import os
import shutil
from psycopg2._psycopg import DatabaseError
from file_store.settings import BASE_DIR


class ORM_files:

    @classmethod
    def _get_file_id(cls, file):
        with open(file, "r") as file:
            file_id = id(file)
        return file_id

    @classmethod
    def _create_connection(cls):
        config = configparser.ConfigParser()
        config.read(os.path.join(BASE_DIR, 'file_store', 'config.ini'))
        db_info = config['DATABASE']
        connection = psycopg2.connect(user=db_info['USERNAME'],
                                      password=db_info['PASSWORD'],
                                      host=db_info['HOST'],
                                      port=db_info['PORT'],
                                      database=db_info['DB'])
        return connection

    @classmethod
    def _create_bd(cls):
        sql = """CREATE TABLE IF NOT EXISTS  files_descriptor (
        file_id serial PRIMARY KEY,
        file_name VARCHAR ( 50 ) UNIQUE NOT NULL,
        additional_date DATE NOT NULL DEFAULT CURRENT_DATE,
        new_path_to_file VARCHAR ( 255 ) UNIQUE NOT NULL,
        old_path_to_file VARCHAR ( 255 ) UNIQUE
        );"""

        with cls._create_connection() as connection:
            cursor = connection.cursor()
            cursor.execute(sql)
            connection.commit()

    @classmethod
    def _reg_and_copy(cls, files, path_to_out, path_to_stream):
        for file in files:
            new_path = path_to_out + '/' + file
            old_path = path_to_stream + '/' + file
            sql = """INSERT INTO files_descriptor 
               (file_name, new_path_to_file, old_path_to_file) 
                VALUES ('{file_name}', '{new}', '{old}');""".format(file_name=file,
                                                                    new=new_path,
                                                                    old=old_path)
            with cls._create_connection() as connection:
                try:
                    cursor = connection.cursor()
                    cursor.execute(sql)
                    shutil.copy2(old_path, new_path)
                    connection.commit()
                except DatabaseError:
                    connection.rollback()

    @classmethod
    def copy(cls):

        # parse conf
        config = configparser.ConfigParser()
        config.read(os.path.join(BASE_DIR, 'file_store', 'config.ini'))
        # print("\n\n", config.read_file, "\n\n")
        path_to_stream = config['IN']['PATH']
        path_to_out = config['OUT']['PATH']

        # get list of all files
        files = []

        for _, _, filenames in os.walk(path_to_stream):
            files.extend(filenames)
            break

        if not files:
            print("Path is not validated")

        # create db if not exist
        cls._create_bd()

        # copy and registry
        cls._reg_and_copy(files, path_to_out, path_to_stream)

    @classmethod
    def get_all_document(cls):
        sql_names = """ SELECT file_name FROM files_descriptor; """
        # sql_urls = """ SELECT old_path_to_file FROM files_descriptor; """
        with cls._create_connection() as connection:
            cursor = connection.cursor()
            cursor.execute(sql_names)
            names = [i[0] for i in cursor.fetchall()]
            names = list(map(lambda x: "media/" + str(x), names))
            # cursor.execute(sql_urls)
            # urls = [i[0] for i in cursor.fetchall()]
        return names
