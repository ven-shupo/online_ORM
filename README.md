# online_ORM
[![pipeline status](https://gitlab.com/ven-shupo/online_ORM/badges/main/pipeline.svg)](https://gitlab.com/ven-shupo/online_ORM/-/commits/main)
### Установка
- git clone https://github.com/ven-shupo/online_ORM/
- sudo docker-compose up --build -d  - сборка и запуск двух контейнеров
- sudo docker-compose exec web python manage.py migrate - применяем миграции 
- sudo docker-compose exec web python start_descriptor.py - запускаем приложение по отслеживанию и архивированию медиа файлов  
- sudo docker-compose exec web python manage.py createsuperuser - создаем администратора с разрешением загрузки контента  
### Routs  
- http://localhost:8000/ - домашняя страница  
![image](https://user-images.githubusercontent.com/73471686/120992439-465fd800-c78b-11eb-990b-9f187ed96ec2.png)
- http://localhost:8000/uploads/ - страница для загрузки контента (доступна только для авторизованных пользователей)   
![image](https://user-images.githubusercontent.com/73471686/120992634-78713a00-c78b-11eb-88f8-42aa3b283db8.png)
- http://localhost:8000/accounts/login/ && http://localhost:8000/accounts/logout/ - страницы для входы и выхода из аккаунта администратора  
![image](https://user-images.githubusercontent.com/73471686/120991911-c0439180-c78a-11eb-8254-78995274426b.png)
### Детали 
Для проверки работы программы по архивированию медиа файлов, достаточно выполнить следющие команды:  
- sudo docker-compose exec db psql --username=django_user --dbname=django_db - заходим в бд внутри контейнера  
- SELECT * FROM files_descriptor; - выводим все содержимое бд с описанием медиа файлов  
![image](https://user-images.githubusercontent.com/73471686/120993904-a86d0d00-c78c-11eb-885e-fca32064db3e.png)   
Пример работы gitlab CI:  
![image](https://user-images.githubusercontent.com/73471686/121159430-db2e0880-c853-11eb-89c3-215836d1c4bc.png)




 
